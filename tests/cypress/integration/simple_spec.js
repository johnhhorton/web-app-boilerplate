describe('Login Page', function() {
  it('Goes to login page', function() {
    cy.visit('http://localhost:8000/home')
    cy.contains('Log In')
    expect(true).to.equal(true)
  })

  it('Goes to login page if not authenticated', function() {
    cy.visit('http://localhost:8000/home#/account')
    cy.contains('Log In')
    expect(true).to.equal(true)
  })

  it('Can go to register', function() {
    cy.visit('http://localhost:8000/home#/account')
    cy.contains("Don't have an account?").click()
    cy.url().should('include', 'register')

  })
})