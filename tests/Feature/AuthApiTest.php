<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Mail\EmailVerification;
use App\Mail\PasswordReset;

class AuthApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function guestCanRegister()
    {
        Mail::fake();
        $userInput = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword'

        ];

        $this
            ->json(
                'POST',
                '/api/auth/register',
                $userInput
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'first',
                'last',
                'email_token'
            ])
            ->assertJson([
                'first' => 'testfirst',
                'last' => 'testlast',
                'email' => 'testemail@gmail.com',
            ]);
    }

    /**
     * @test
     */
    public function RegisteredGuestReceivesVerificationEmail()
    {
        Mail::fake();

        $userInput = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword'
        ];

        $this
            ->json(
                'POST',
                '/api/auth/register',
                $userInput
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'first',
                'last',
                'email_token'
            ])
            ->assertJson([
                'first' => 'testfirst',
                'last' => 'testlast',
                'email' => 'testemail@gmail.com'
            ]);

        $user = User::where('email', 'testemail@gmail.com')->first();
        $this->assertEquals($user->verified, 0);

        // Assert a message was sent to the given users...
        Mail::assertSent(EmailVerification::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }

    /**
     * @test
     */
    public function RegisteredGuestCanVerifyAccount()
    {
        Mail::fake();

        $userInput = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword'
        ];

        $this
            ->json(
                'POST',
                '/api/auth/register',
                $userInput
            )
            ->assertStatus(200);

        $user = User::where('email', 'testemail@gmail.com')->first();
        $this->assertEquals($user->verified, 0);
        $token = $user->email_token;
        $this
            ->json(
                'POST',
                "/api/auth/verifyemail/{$token}"
            )
            ->assertStatus(200);
        $user = User::where('email', 'testemail@gmail.com')->first();
        $this->assertEquals($user->verified, 1);
        $this->assertEquals($user->email_token, null);
    }

    /**
     * @test
     */
    public function RegisteredGuestCannotVerifyMoreThanOnce()
    {
        Mail::fake();

        $userInput = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword'
        ];

        $this
            ->json(
                'POST',
                '/api/auth/register',
                $userInput
            )
            ->assertStatus(200);

        $user = User::where('email', 'testemail@gmail.com')->first();
        $this->assertEquals($user->verified, 0);
        $token = $user->email_token;
        $this
            ->json(
                'POST',
                "/api/auth/verifyemail/{$token}"
            )
            ->assertStatus(200);
        $user = User::where('email', 'testemail@gmail.com')->first();
        $this->assertEquals($user->verified, 1);
        $this->assertEquals($user->email_token, null);
        $this
            ->json(
                'POST',
                "/api/auth/verifyemail/{$token}"
            )
            ->assertStatus(500);
    }

    /**
     * @test
     */
    public function registerShouldFailWithoutData()
    {
        $this
            ->json(
                'POST',
                '/api/auth/register',
                []
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'first',
                    'last',
                    'email',
                    'password'
                ],
            ]);
    }

    /**
     * @test
     */
    public function registerShouldValidatePasswordsMatch()
    {
        $userInput = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword2'

        ];

        $this
            ->json(
                'POST',
                '/api/auth/register',
                $userInput
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'password'
                ],
            ]);
    }

    /**
     * @test
     */
    public function registerShouldValidateEmail()
    {
        $userInput = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'notanemail',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword2'

        ];

        $this
            ->json(
                'POST',
                '/api/auth/register',
                $userInput
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email'
                ],
            ]);
    }

    /**
     * @test
     */
    public function userCanLogin()
    {
        $newUser = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];
        $userInput = [
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];

        factory(\App\User::class)->make($newUser)->save();
        $response = $this->json(
            'POST',
            '/api/auth/login',
            $userInput
        );
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    /**
     * @test
     */
    public function userCanGetMe()
    {
        $newUser = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];
        $userInput = [
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];
        $expectedMe = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com'
        ];

        factory(\App\User::class)->make($newUser)->save();

        $token = $this
            ->json(
                'POST',
                '/api/auth/login',
                $userInput
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->json()['access_token'];

        $this
            ->json('POST', '/api/auth/me', [], [
                'Authorization' => 'Bearer ' . $token,
            ])
            ->assertStatus(200)
            ->assertJson($expectedMe);
    }

    /**
     * @test
     */
    public function guestCannotGetMe()
    {
        $this->json('POST', '/api/auth/me', [])
//        ->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);
    }

    /**
     * @test
     */
    public function userCanLogout()
    {
        $newUser = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];
        $userInput = [
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];
        $expectedMe = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com'
        ];

        factory(\App\User::class)->make($newUser)->save();

        $token = $this
            ->json(
                'POST',
                '/api/auth/login',
                $userInput
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ])
            ->json()['access_token'];

        $this
            ->json('POST', '/api/auth/logout', [], [
                'Authorization' => 'Bearer ' . $token,
            ])
            ->assertStatus(200)
            ->assertJsonStructure(['message']);

        $this
            ->json('POST', '/api/auth/me', [], [
                'Authorization' => 'Bearer ' . $token,
            ])
            ->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);
    }

    /**
     * @test
     */
    public function guestCanRequestPasswordReset()
    {
        Mail::fake();
        $newUser = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];

        factory(\App\User::class)->make($newUser)->save();

        $this
            ->json(
                'POST',
                '/api/auth/requestpasswordreset/',
                ['email' => $newUser['email']]
            )
            ->assertStatus(200);
        $user = User::where('email', $newUser['email'])->first();
        $this->assertNotNull($user->password_reset_token);

        // Assert a message was sent to the given users...
        Mail::assertSent(PasswordReset::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }

    /**
     * @test
     */
    public function guestCanValidatePasswordResetToken()
    {
        Mail::fake();
        $newUser = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];

        factory(\App\User::class)->make($newUser)->save();

        $this
            ->json(
                'POST',
                '/api/auth/requestpasswordreset/',
                ['email' => $newUser['email']]
            )
            ->assertStatus(200);
        $user = User::where('email', $newUser['email'])->first();
        $this->assertNotNull($user->password_reset_token);
        $this
            ->json(
                'POST',
                "/api/auth/verifyresettoken/?token={$user->password_reset_token}"
            )
            ->assertStatus(200);

        // Assert a message was sent to the given users...
        Mail::assertSent(PasswordReset::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }

    /**
     * @test
     */
    public function guestCanResetPasswordWithValidToken()
    {
        Mail::fake();
        $newUser = [
            'first' => 'testfirst',
            'last' => 'testlast',
            'email' => 'testemail@gmail.com',
            'password' => 'testpassword',
        ];

        factory(\App\User::class)->make($newUser)->save();

        $this
            ->json(
                'POST',
                '/api/auth/requestpasswordreset/',
                ['email' => $newUser['email']]
            )
            ->assertStatus(200);
        $user = User::where('email', $newUser['email'])->first();
        $this->assertNotNull($user->password_reset_token);
        $this
            ->json(
                'POST',
                "/api/auth/verifyresettoken/?token={$user->password_reset_token}"
            )
            ->assertStatus(200);
        $this
            ->json(
                'POST',
                "/api/auth/resetpassword/",
                [
                    'email' => $newUser['email'],
                    'token' => $user->password_reset_token,
                    'password' => 'newpassword',
                    'password_confirmation' => 'newpassword'
                ]
            )
            ->assertStatus(200);

        $this
            ->json(
                'POST',
                '/api/auth/login',
                [
                    'email' => 'testemail@gmail.com',
                    'password' => 'newpassword',
                ]
            )
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ]);

        // Assert a message was sent to the given users...
        Mail::assertSent(PasswordReset::class, function ($mail) use ($user) {
            return $mail->hasTo($user->email);
        });
    }
}
