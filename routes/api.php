<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'AuthController@login');
Route::post('auth/register', 'AuthController@register');
Route::post('auth/verifyemail/{token}', 'AuthController@verify');
Route::post('auth/resendverification/', 'AuthController@resendVerification');
Route::post('auth/requestpasswordreset/', 'AuthController@requestPasswordReset');
Route::post('auth/verifyresettoken/', 'AuthController@validatePasswordResetToken');
Route::post('auth/resetpassword', 'AuthController@passwordReset');

//Any routes that need to be protected by auth
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('auth/refresh', 'AuthController@refresh');
    Route::post('auth/logout', 'AuthController@logout');
    Route::post('auth/me', 'AuthController@me');
    Route::post('auth/update', 'AuthController@update');
    Route::get('notifications/', 'NotificationController@index');
    Route::get('notifications/clear', 'NotificationController@clearNotifications');
    Route::get('notifications/{id}', 'NotificationController@destroy');

    Route::resource('projects', 'ProjectController', [
        'only' => [
            'index',
            'show',
            'store',
            'update',
            'destroy'
        ]
    ]);

    Route::resource('task', 'TaskController', [
        'only' => [
            'index',
            'show',
            'store',
            'update',
            'destroy'
        ]
    ]);

});

