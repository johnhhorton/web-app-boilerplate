import axios from 'axios'
import debug from 'debug'

const debugBuild = debug('ResourceModule:build')
const debugRun = debug('ResourceModule:run')

function condenseIds (key = false, items = false) {
  if (items[key]) {
    console.log('single response', items)
    return items[key]
  } else if (items[0] && items[0][key]) {
    console.log('array of models', items)
    return items.reduce((accumulator, currentItem) => {
      return accumulator.concat(currentItem[key])
    }, [])
  }

  return false
}

function relationshipsHander ({relationships, store, response}) {
  if (relationships && Array.isArray(relationships) && relationships.length > 0) {
    return Promise.all(
      relationships.map(({
                           module = false,
                           relationships = false,
                           relationshipKey = false
                         }) => {
        let id = condenseIds(relationshipKey, response.data)
        console.log('post condenseIds', id)
        return loadHelper({
          module,
          store,
          id,
          relationships
        })
      }))
  } else {
    return response
  }
}

export function loadHelper ({
                              module = false,
                              store = false,
                              id = false,
                              relationships = false,
                              relationshipKey = false
                            }) {
  debugRun('loadHelper(): start')
  if (!module || !store) throw new Error('loadHelper must be provided (module (string:module name), store')
  let action = (id !== false) ? 'show' : 'get'
  if (Array.isArray(id)) action = 'get'
  debugRun('loadHelper(): setup end (next runs dispatches)')
  return store.dispatch(`${module}/${action}`, {id})
    .then((response) => {
      return relationshipsHander({
        response,
        relationships,
        store
      })
    })
}

export default function ({route, vuexName}, baseurl = '') {
  debugBuild(`Start build: ${route}, vuexName: ${vuexName}`)
  if (!route || !vuexName) throw new Error('Resource constructor: resource requires route and vuexName')
  // let module = store
  let module = {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {}
  }
  // STATE
  module.state[vuexName] = []
  // ON PAGE CRUD
  module.getters[`get`] = state => id => {
    let returnItem

    if (Array.isArray(id)) {
      debugRun(`getter ${vuexName}/get id:`, id, returnItem)
      returnItem = state[vuexName].filter(storeItem => {
        return id.indexOf(storeItem.id) > -1
      })
    } else {
      returnItem = state[vuexName].find(site => site.id === id)
    }

    debugRun(`getter ${vuexName}/get id:`, id, returnItem)
    return returnItem
  }
  module.mutations[`add`] = (state, data) => {
    debugRun(`mutation ${vuexName}/add data`, data)
    if (Array.isArray(data)) {
      state[vuexName] = state[vuexName].map(item => {
        // merging new items
        return data.find(newItem => newItem.id === item.id) || item
      }).concat(data.filter((item) => {
        // removing duplicates
        return !state[vuexName].find(oldItem => oldItem.id === item.id)
      }))
    } else if (state[vuexName].find(oldItem => oldItem.id === data.id)) {
      state[vuexName] = state[vuexName].map(item => {
        return (item.id === data.id) ? data : item
      })
    } else {
      state[vuexName] = state[vuexName].concat(data)
    }
  }
  module.mutations[`change`] = (state, data) => {
    debugRun(`mutation ${vuexName}/change data`, data)
    state[vuexName] = state[vuexName].map(resource => {
      if (resource.id === data.id) {
        return data
      }
      return resource
    })
  }
  module.mutations[`remove`] = (state, data) => {
    debugRun(`mutation ${vuexName}/remove data`, data)
    let id = (typeof data !== 'object') ? data : data.id
    state[vuexName] = state[vuexName].filter(resource => resource.id !== id)
  }
  module.mutations[`clear`] = (state, data) => {
    debugRun(`mutation ${vuexName}/clear data`, data)
    state[vuexName] = []
  }
  // SERVER CRUD
  module.actions[`get`] = ({dispatch, commit, state}, {id = false} = {}) => {
    debugRun(`action ${vuexName}/get id`, id)
    let query = (id) ? `?filter=${id.join(',')}` : ''
    return axios.get(`${baseurl}${route}${query}`)
      .then((response) => {
        commit(`add`, response.data)
        return response
      })
      .catch(err => {
        console.log('error', err)
        throw err.response.data
      })
      .then(response => {
        return response
      })
  }

  module.actions[`show`] = ({dispatch, commit, state}, {id = ''}) => {
    debugRun(`action ${vuexName}/show id`, id)
    return axios.get(`${baseurl}${route}/${id}`)
      .then((response) => {
        commit(`add`, response.data)
        return response
      })
      .catch(err => {
        console.log('error', err)
        throw err.response.data
      })
      .then(response => {
        return response
      })
  }
  module.actions[`store`] = ({dispatch, commit, state}, data) => {
    debugRun(`action ${vuexName}/store data`, data)
    return axios.post(`${baseurl}${route}`, data)
      .then((response) => {
        commit(`add`, response.data)
        return response
      })
      .catch(err => {
        console.log('caught error', err.response.data)
        throw err.response.data
      })
      .then(response => {
        return response
      })
  }

  module.actions[`update`] = ({dispatch, commit, state}, data) => {
    debugRun(`action ${vuexName}/update data`, data)
    return axios.put(`${baseurl}${route}/${data.id}`, data)
      .then((response) => {
        commit(`change`, response.data)
        return response
      })
      .catch(err => {
        console.log('caught error', err.response.data)
        throw err.response.data
      })
      .then(response => {
        return response
      })
  }

  module.actions[`destroy`] = ({dispatch, commit, state}, data) => {
    debugRun(`action ${vuexName}/destroy data`, data)
    let id = (typeof data !== 'object') ? data : data.id
    return axios.delete(`${baseurl}${route}/${id}`)
      .then((response) => {
        commit(`remove`, response.data)
        return response
      })
      .catch(err => {
        console.log('caught error', err.response.data)
        throw err.response.data
      })
      .then(response => {
        return response
      })
  }
  debugBuild(`Finish build: vuexName: ${vuexName}`)
  return module
}
