export const deepClone = (obj) => {
  return JSON.parse(JSON.stringify(obj))
}

export function registerFeature () {

}

/**
 * Used by Vue form components to map laravel errors to computed properties.
 *
 * For each mapped error, the template can bind to `[propertyName]Errors`
 *
 * @param moduleStore - form control module handling the form state.
 * @param errors - properties that could be returned as errors to map.
 * @returns ...spread operator compatible list
 */
export function mapLaravelErrors (moduleStore = null, errors = []) {


  //Map property errors to computed
  const computed = errors.reduce((accumulator, value) => {
    accumulator[`${value}Errors`] = function () {
      let module = this.$store.state[moduleStore]
      // if(!module){
      //   return []
      // }
      if (module.errors && module.errors.errors && module.errors.errors[value]) {
        return module.errors.errors[value]
      }
      return []
    }

    return accumulator
  }, {})

  //Map the general error message stored in the module's error state.
  computed.errorMessage = function () {
    let module = this.$store.state[moduleStore]
    // if(!module){
    //   return null
    // }
    if (module.errors && module.errors.message) {
      return module.errors.message
    }
    return null
  }

  return computed
}

/**
 * Used by Vue form components to provide basic form methods
 *
 *
 * @param moduleStore - form control module handling the form state.
 * @param errors - properties that could be returned as errors to map.
 * @returns ...spread operator compatible list
 */
export function mapFormMethods (moduleName) {
  return {
    submitForm () {
      this.$store.dispatch(`${moduleName}/changeFormData`, this.formData)
      return this.$store.dispatch(`${moduleName}/submitForm`)
    }
  }
}

/**
 * Used by Vue form components. Maps form status messages into the template.
 *
 * @param moduleStore - form control module handling the form state.
 * @returns ...spread operator compatible list
 */
export function mapFormStatus (moduleStore = null) {
  return {
    //Returns the status message to the submission button (ie: Save, saving, failed)
    submitMessage () {
      const module = this.$store.state[moduleStore]
      if (typeof module.submitMessage !== 'undefined') {
        return module.submitMessage
      } else {
        return 'Submit message not set'
      }
    },
    //Returns whether or not the form is currently submitting. Used for indeterminate progress indicators.
    submitting () {
      const module = this.$store.state[moduleStore]
      if (typeof module.submitting !== 'undefined') return module.submitting
      return false
    },
    //Changes the submit button color depending on form submission status.
    submitColor () {
      const module = this.$store.state[moduleStore]
      if (typeof module.submitColor !== 'undefined') return module.submitColor
      return 'primary'
    }
  }
}

/**
 * Used by Vuex Form modules to set up their default state.
 * */
export const vuexFormState = {
  errors: {},
  submitMessage: 'Save', //changed internally, use default in components
  submitColor: 'primary', //changed internally, use default in components
  submitMessageSaving: 'Saving...', //component changeable
  submitMessageSuccess: 'Saved!',//component changeable
  submitMessageDefault: 'Save',//component changeable
  submitSuccessColor: 'success',//component changeable
  submitColorSaving: 'disabled',//component changeable
  submitColorDefault: 'primary',//component changeable
  redirect: false,
  submitting: false,
}

/**
 * /used by Vuex Form modules to set up their common mutations
 */
export const vuexFormMutations = {
  //Set the form data state
  setFormData (state, formData) {
    state.formData = {...formData}
  },
  //Set the form's Laravel errors
  setErrors (state, error) {
    state.errors = error
  },
  //used internally to be able to revert submit message back to the one originally set.
  SetSubmitMessageDefault (state) {
    state.submitMessage = state.submitMessageDefault
  },
  startSubmitStatus (state) {
    state.submitMessage = state.submitMessageSaving
    state.submitting = true
    state.submitColor = state.submitColorSaving
  },
  succeedSubmitStatus (state) {
    state.submitMessage = state.submitMessageSuccess
    state.submitting = false
    state.submitColor = state.submitSuccessColor
  },
  failSubmitStatus (state) {
    state.submitMessage = state.submitMessageDefault
    state.submitting = false
    state.submitColor = state.submitColorDefault
  },
  resetSubmitStatus (state) {
    state.submitMessage = state.submitMessageDefault
    state.submitting = false
    state.submitColor = state.submitColorDefault
  }
}

/**
 * /used by Vuex Form modules to set up their common actions
 */
export const vuexFormActions = {
  //Rather than live mapping data from the component. This function is called to set the formData in state before submitting.
  changeFormData ({dispatch, commit, getters, rootGetters}, formData) {
    commit('setFormData', formData)
  },
  //Helper used by the module to set a successful state.
  setFormSuccess ({dispatch, commit, getters, rootGetters}, resetFomData = false) {
    commit('succeedSubmitStatus')
    commit('setErrors', {})
    if (resetFomData) commit('setFormData', resetFomData)
    setTimeout(() => {
      commit('resetSubmitStatus')
    }, 1000)
  },
  //Helper used by the module to set a failure state.
  setFormFailure ({dispatch, commit, getters, rootGetters}, laravelErrorResponse) {
    commit('failSubmitStatus')
    commit('setErrors', laravelErrorResponse)
    //Resets the form status after showing the error to the user.
    setTimeout(() => {
      commit('resetSubmitStatus')
    }, 3000)
  }
}

export function makeVuexForm ({name, namespaced = true, state = {}, mutations = {}, getters = {}, actions = {}}) {
  if (!name) {
    throw new Error('makeVuexForm expects that the Vuex Module has a \'name\' property')
  }
  //namespaced, state,mutations, getters, actions
  return {
    name,
    namespaced,
    state: {...vuexFormState, ...state},
    mutations: {...vuexFormMutations, ...mutations},
    getters,
    actions: {...vuexFormActions, ...actions},
  }
}

export function formComponentMixin (formModule) {
  return {
    data () {
      return {
        formData: {...formModule.state.formData}
      }
    },
    created () {
      this.$store.registerModule(formModule.name, formModule)
      //workaround. register module(_,_,{preserveState:false}) doesn't work with nested objects
      this.$store.state[formModule.name].defaultState = this.$store.state[formModule.name].formData
    },
    destroyed () {
      //workaround. register module(_,_,{preserveState:false}) doesn't work with nested objects
      this.$store.state[formModule.name].formData = this.$store.state[formModule.name].defaultState
      this.$store.unregisterModule(formModule.name)
    },
    methods: {
      ...mapFormMethods(formModule.name),
    },
    computed: {
      ...mapLaravelErrors(formModule.name, Object.keys(formModule.state.formData)),
      ...mapFormStatus(formModule.name),
    }
  }
}