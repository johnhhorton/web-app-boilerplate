import Vue from 'vue'
import storeSetup from '../store' //Application storage
import routerSetup from '../router' //Application routing system
import { sync } from 'vuex-router-sync' //Syncs router data with vuex

/**
 * Creates the Vuex store and Vue-Router
 * Registers passed features routes, modules, plugins.
 * Runs feature setup functions
 * @param features
 */
export default function setupStoreAndRouter ({features = []}) {
  let modules = {}
  let routes = []
  let plugins = []

  //Combine all feature modules, routes, and plugins
  features.forEach(feature => {
    if (feature.modules) {
      modules = {...modules, ...feature.modules}
    }
    if (feature.routes) {
      routes = [...routes, ...feature.routes]
    }
    if (feature.plugins) {
      plugins = plugins.concat(feature.plugins)
    }
  })

  //Setup store and include feature modules
  const store = storeSetup({
    ...modules
  })

  //Set up router and include feature routes
  const router = routerSetup([
    ...routes
  ], store)

  //Add all feature plugins to global Vue object
  plugins.forEach((plugin) => {Vue.use(plugin, {store, router})})

  //Run setup functions for each feature
  features.forEach(feature => {
    if (feature.setup) {
      for (var setupFunction in feature.setup) {
        feature.setup[setupFunction]({store, router})
      }
    }
  })

  //Synchronize store and router using routeModule
  sync(store, router, {moduleName: 'routeModule'})

  return {store, router}
}