import axios from 'axios'
import Debug from 'debug'

let modelCache = []

function getCache (id = -1) {
  return modelCache.find(model => model.id === id)
}

function getCacheArray (ids = []) {
  let foundModels = modelCache.filter(model => ids.indexOf(model.id) > -1)
  if (foundModels.length !== ids.length) return false
  return foundModels
}

function addCache (model = {id: -1}) {
  modelCache = modelCache.filter(cachedModel => cachedModel.id !== model.id)
  modelCache = modelCache.concat(model)
}

/**
 * Generates a Vue plugin to use as a model service.
 * To use, call this.$modelName.method()
 * Methods are: index, show, create, update, destroy (map to laravel)
 * @param name {string} Vue name for model
 * @param url {string} base url of model
 * @returns {VuePlugin}
 */
export function createModelServicePlugin ({name, url}) {
  const debug = Debug(`ModelService:${name}`)
  return {
    install (Vue, options) {
      //TODO: Add prototype that allows for dynamic registration of $store modules.
      //TODO: replace direct api calls with a store interface. Gotta be speedy =)



      Vue.prototype[`$${name}`] = {
        index (ids = []) {
          let query = (ids.length > 0) ? `?filter=${ids.join(',')}` : ''
          return axios.get(`${url}${query}`)
            .then(({data}) => {
              debug(`index()`, data)
              return data
            })
            .catch(err => {
              debug(`index()`, err)
              throw err.response.data
            })
        },
        show (id) {
          return axios.get(`${url}/${id}`)
            .then(({data}) => {
              debug(`show()`, data)
              return data
            })
            .catch(err => {
              debug(`show()`, err)
              throw err.response.data
            })
        },
        store (model) {
          return axios.post(`${url}`, model)
            .then(({data}) => {
              debug(`store()`, data)
              return data
            })
            .catch(err => {
              debug(`store()`, err)
              throw err.response.data
            })
        },
        update (model) {
          return axios.put(`${url}/${model.id}`, model)
            .then(({data}) => {
              debug(`update()`, data)
              return data
            })
            .catch(err => {
              debug(`update()`, err)
              throw err.response.data
            })
        },
        destroy (id) {
          return axios.delete(`${url}/${id}`)
            .then(({data}) => {
              debug(`destroy()`, data)
              return data
            })
            .catch(err => {
              debug(`destroy()`, err)
              throw err.response.data
            })
        }
      }
    }
  }
}