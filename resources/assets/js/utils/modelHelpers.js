export default function modelMixin(model){
  let module = `${model}Module`
  return{
    methods:{
      [`${model}GetAll`]: function () {
        return this.$store.dispatch(`${module}/get`)
      },
      [`${model}GetSingle`]: function (id) {
        return this.$store.dispatch(`${module}/show`, {id})
      }
    },
    computed:{
      [`${model}All`]: function () {
        return this.$store.state[module][model]
      },
      [`${model}Single`]: function (id) {
        return (id) => this.$store.getters[`${module}/get`](id)
      }
    }
  }
}