__webpack_public_path__ = window.rooturl + '/js/'
import debug from 'debug' //console debugging tool
import Vue from 'vue'
import Vuetify from 'vuetify' //Vuetify UI Framework
import 'vuetify/dist/vuetify.min.css' //css for Vutify framework
import App from './App.vue' //Application entrypoint component
import setupStoreAndRouter from './utils/setup-store-and-router'
//Features
import applicationSidebar from './features/application-sidebar'
import authentication from './features/authentication'
import account from './features/account'
import projects from './features/projects'
import tasks from './features/tasks'
import notifications from './features/notifications'

//Setup debugging
debug.log = console.log.bind(console)
debug.enable('*')

Vue.config.productionTip = false

Vue.use(Vuetify) //Add Vuetify plugin to register global components

//Handles the creation of store and router, and includes features.
let {store, router} = setupStoreAndRouter({
  features: [
    applicationSidebar,
    authentication,
    account,
    projects,
    tasks,
    notifications
  ]
})

store.dispatch('authModule/startupCheck')

//Start the Vue Application
new Vue({
  store,
  router,
  el: '#app',
  template: '<App/>',
  components: {App},
  created () {
    //Adds the menus to Vuex. TODO: Needs to be moved.
    this.$sidebar.create({
      name: 'primary',
      items: [
        {title: 'Home', icon: 'home', to: {name: 'home'}},
        {title: 'Projects', icon: 'work', to: {name: 'projects'}}
      ]
    })
  }
})
