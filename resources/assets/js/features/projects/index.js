import routes from './routes'
import projectsService from './plugins/projects-service'

export default {
  routes,
  modules: {
  },
  plugins: [
    projectsService
  ],
  setup:{
    createSidebars({store}){
      store.commit('sidebarModule/create',{
        name: 'project',
        items: [
          {title: 'Home', icon: 'home', to: {name: 'home'}},
          {title: 'Projects2', icon: 'work', to: {name: 'projects'}}
        ]
      })
    }
  }
}