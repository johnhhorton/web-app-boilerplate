const ProjectsPage = () => import(/* webpackChunkName: "projects-page" */ './pages/projects-page')
const ProjectCreatePage = () => import(/* webpackChunkName: "project-create-page" */ './pages/project-create-page')
const ProjectSinglePage = () => import(/* webpackChunkName: "project-single-page" */ './pages/project-single-page')
const ProjectEditPage = () => import(/* webpackChunkName: "project-edit-page" */ './pages/project-edit-page')

export default [
  {path: '/projects/create', name: 'project-create', component: ProjectCreatePage, meta: {auth: true}},
  {path: '/projects/:id', name: 'project-single', component: ProjectSinglePage, meta: {auth: true, sidebar:'project'}},
  {path: '/projects/:id/edit', name: 'project-edit', component: ProjectEditPage, meta: {auth: true}},
  {path: '/projects', name: 'projects', component: ProjectsPage, meta: {auth: true}},
]