import { createModelServicePlugin } from 'utils/modelService'

export default createModelServicePlugin({
  name: 'projects',
  url: `${window.rooturl}/api/projects`
})