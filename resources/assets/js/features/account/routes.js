const AccountPage = () => import(/* webpackChunkName: "account-page" */ './pages/account-page')

export default [
  {path: '/account', name: 'account', component: AccountPage, meta: {auth: true}},
]