import sidebarModule from './store/sidebar-module'
import sidebarPlugin from './plugins/sidebar'
export default {
  modules:{
    sidebarModule
  },
  plugins:[
    sidebarPlugin
  ]
}