/**
 * Sidebar Vue Plugin
 * Methods:
 * this.$sidebar.setCurrent("sidebar-name")
 * this.$sidebar.getCurrent("sidebar-name")
 * this.$sidebar.createCurrent("sidebar-name")
 * this.$sidebar.create({name:"", items:[{title:"",to:{name:""},icon:""}]})
 */
export default {
  install (Vue, {store, router}) {
    Vue.prototype.$sidebar = {
      //Set current sidebar by name
      setCurrent (name) {
        store.commit('sidebarModule/setCurrent', name)
      },

      //get current sidebar object
      getCurrent () {
        return store.getters['sidebarModule/getCurrentSidebar']
      },

      //set current sidebar object directly for dynamic generation
      createCurrent (sidebar) {
        let {
          name,
          items: []
        } = sidebar
        store.commit('sidebarModule/createCurrent', sidebar)
      },

      //Create new sidebar and add to sidebars list to be referenced by name
      create (sidebar) {
        let {
          name,
          items: []
        } = sidebar
        store.commit('sidebarModule/create', sidebar)
      }
    }
  }
}