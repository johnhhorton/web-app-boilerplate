export default {
  namespaced: true,
  name: 'sidebarModule',
  state: {
    currentSidebar: false,
    sidebars: []
  },
  mutations: {
    create (state, sidebar) {
      state.sidebars = state.sidebars.concat(sidebar)
    },
    setCurrent (state, sidebarName) {
      if (!sidebarName) {
        state.currentSidebar = false
      } else {
        state.currentSidebar = state.sidebars.find(sidebar => sidebar.name === sidebarName)
      }
    },
    createCurrent (state, sidebar) {
      state.currentSidebar = sidebar
    }
  },
  getters: {
    getCurrentSidebar (state) {
      return state.currentSidebar
    }
  }
}