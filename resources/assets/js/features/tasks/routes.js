// tasks/routes.js
const TaskSinglePage = () => import(/* webpackChunkName: "task-single-page" */ './pages/task-single-page')
const TaskCreatePage = () => import(/* webpackChunkName: "task-create-page" */ './pages/task-create-page')

export default [
  {path: '/task/create', name: 'task-create', component: TaskCreatePage, meta: {auth: true}},
  {path: '/task/:id', name: 'task-single', component: TaskSinglePage, meta: {auth: true}}
]