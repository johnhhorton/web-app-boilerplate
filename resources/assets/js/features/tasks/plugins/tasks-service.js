import { createModelServicePlugin } from 'utils/modelService'

export default createModelServicePlugin({
  name: 'tasks',
  url: `${window.rooturl}/api/task`
})