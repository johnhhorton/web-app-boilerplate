// tasks/index.js
import routes from './routes'
import tasksService from './plugins/tasks-service'

export default {
  routes,
  plugins: [
    tasksService
  ]
}