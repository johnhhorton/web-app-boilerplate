
import notificationsModule from './store/notifications'

export default {
  modules:{
    notificationsModule
  }
}