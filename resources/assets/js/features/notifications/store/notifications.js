import axios from 'axios'

let root = window.rooturl
export default {
  name: 'notificationsModule',
  namespaced: true,
  state: {
    notifications: []
  },
  mutations: {
    setNotifications (state, notifications) {
      state.notifications = notifications
    }
  },
  actions: {
    index ({dispatch, commit, state}) {
      return axios.get(`${root}/api/notifications/`)
        .then((response) => {
          commit('setNotifications', response.data)
        })
        .catch(({response: {data}}) => {
          throw data
        })
    },
    destroy ({dispatch, commit, state}, {id}) {
      return axios.get(`${root}/api/notifications/${id}`)
        .then((response) => {
          commit('setNotifications', response.data)
        })
        .catch(({response: {data}}) => {
          throw data
        })
    },
    destroyAll ({dispatch, commit, state}) {
      return axios.get(`${root}/api/notifications/clear`)
        .then((response) => {
          commit('setNotifications', response.data)
        })
        .catch(({response: {data}}) => {
          throw data
        })
    }
  }
}