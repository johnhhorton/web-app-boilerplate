import routes from './routes'
import authModule from './store/auth'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import localStore from 'store'

export default {
  routes,
  modules: {
    authModule
  },
  setup:{
    authCheck ({store, router}) {
      axios.interceptors.response.use((response) => {
        return Promise.resolve(response)
      }, (error) => {
        if (error.response.status === 401 && error.response.data.message === 'Unauthenticated') {
          let response = error.response
          let jwt = localStore.get('jwt')
          let token = jwtDecode(jwt)
          let currentTime = Date.now() / 1000

          if (currentTime > token.exp) {
            store.dispatch('authModule/logout')
            router.push({name: 'login'})
          }
        }
        return Promise.reject(error)
      })
    }
  }

}