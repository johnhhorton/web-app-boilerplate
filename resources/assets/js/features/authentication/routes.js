const LoginPage = () => import(/* webpackChunkName: "login-page" */ './pages/login-page')
const RegisterPage = () => import(/* webpackChunkName: "register-page" */ './pages/register-page')
const EmailVerificationPage = () => import(/* webpackChunkName: "email-verification-page" */ './pages/email-verification-page')
const resetPasswordPage = () => import(/* webpackChunkName: "reset-password-page" */ './pages/reset-password-page')
const requestPasswordResetPage = () => import(/* webpackChunkName: "request-password-reset-page" */ './pages/request-password-reset-page')

export default [
  {path: '/auth/login', name: 'login', component: LoginPage, meta: {auth: false, layout: 'auth'}},
  {path: '/auth/register', name: 'register', component: RegisterPage, meta: {auth: false, layout: 'auth'}},
  {path: '/auth/verify', name: 'verifyemail', component: EmailVerificationPage, meta: {auth: false, layout: 'auth'}},
  {
    path: '/auth/password-reset',
    name: 'passwordreset',
    component: resetPasswordPage,
    meta: {auth: false, layout: 'auth'}
  },
  {
    path: '/auth/password-reset-request',
    name: 'requestpasswordreset',
    component: requestPasswordResetPage,
    meta: {
      auth: false, layout: 'auth'
    }
  },
]