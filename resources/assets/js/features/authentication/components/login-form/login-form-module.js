/**
 * Auth Login Form Module
 *
 * Used to manage the submission state of the login form vue component
 */

import { makeVuexForm } from 'utils/utils'

export default makeVuexForm({
  name:'loginFormModule',
  namespaced:true,
  state: {
    submitMessageSaving: 'Logging In...',
    submitMessageSuccess: 'Successfully Logged In',
    submitMessage: 'Log In',
    submitMessageDefault: 'Log In',
    formData: {
      email: '',
      password: ''
    }
  },
  actions: {
    submitForm ({dispatch, state, commit, getters, rootGetters}) {
      commit('startSubmitStatus')
      commit('setErrors', {})
      return dispatch('authModule/login', state.formData, {root: true})
        .then((response) => {
          dispatch('setFormSuccess')
          return response
        })
        .catch((error) => {
          dispatch('setFormFailure', error)
          throw error
        })
    }
  }
})
