/**
 * Request Password Reset Form Module
 *
 * Used to manage request password reset form
 */

import { makeVuexForm } from 'utils/utils'

export default makeVuexForm({
  name: 'requestPasswordResetForm',
  namespaced: true,
  state: {
    submitMessageSaving: 'Sending Request',
    submitMessageSuccess: 'Request Sent',
    submitMessage: 'Request Reset',
    submitMessageDefault: 'Request Reset',
    formData: {
      email: ''
    }
  },
  actions: {
    submitForm ({dispatch, state, commit, getters, rootGetters}) {
      commit('startSubmitStatus')
      commit('setErrors', {})
      return dispatch('authModule/requestPasswordReset', state.formData, {root: true})
        .then((response) => {
          dispatch('setFormSuccess')
          return response
        })
        .catch((error) => {
          dispatch('setFormFailure', error)
          throw error
        })
    }
  }
})
