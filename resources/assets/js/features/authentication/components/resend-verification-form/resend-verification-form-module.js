/**
 * Resend Verification Form Module
 *
 * Use to manage resending email verification emails in resend-verification-form.vue
 */

import { makeVuexForm } from 'utils/utils'

export default makeVuexForm({
  name:'resendVerificationFormModule',
  namespaced: true,
  state: {
    submitMessageSaving: 'Attempting to Send Email...',
    submitMessageSuccess: 'Email Sent!',
    submitMessage: 'Send Verification Email Again',
    submitMessageDefault: 'Send Verification Email Again',
    formData: {
      email: '',
    }
  },
  actions: {
    submitForm ({dispatch, state, commit, getters, rootGetters}) {
      commit('startSubmitStatus')
      commit('setErrors', {})
      return dispatch('authModule/resendVerification', state.formData, {root: true})
        .then((response) => {
          dispatch('setFormSuccess')
          return response
        })
        .catch((error) => {
          dispatch('setFormFailure', error)
          throw error
        })
    }
  }
})
