/**
 * Reset Password Form Module
 *
 * Used to manage state of password reset form
 */

import { makeVuexForm } from 'utils/utils'

export default makeVuexForm({
  name: 'resetPasswordFormModule',
  namespaced: true,
  state: {
    submitMessageSaving: 'Changing Password',
    submitMessageSuccess: 'Password Changed',
    submitMessage: 'Change Password',
    submitMessageDefault: 'Change Password',
    formData: {
      email: '',
      password: '',
      password_confirmation: '',
      token: ''
    }
  },
  actions: {
    submitForm ({dispatch, state, commit, getters, rootGetters}) {
      commit('startSubmitStatus')
      commit('setErrors', {})
      return dispatch('authModule/resetPassword', state.formData, {root: true})
        .then((response) => {
          dispatch('setFormSuccess')
          return response
        })
        .catch((error) => {
          dispatch('setFormFailure', error)
          throw error
        })
    }
  }
})
