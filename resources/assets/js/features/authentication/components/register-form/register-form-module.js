/**
 * Register Form Module
 *
 * Used to manage register-form.vue state for the user registration process
 */

import { makeVuexForm } from 'utils/utils'

export default makeVuexForm({
  name:'registerFormModule',
  namespaced: true,
  state: {
    submitMessageSaving: 'Registering...',
    submitMessageSuccess: 'Successfully Registered',
    submitMessage: 'Register',
    submitMessageDefault: 'Register',
    formData: {
      first: '',
      last: '',
      email: '',
      password: '',
      password_confirmation: ''
    }
  },
  actions: {
    submitForm ({dispatch, state, commit, getters, rootGetters}) {
      commit('startSubmitStatus')
      commit('setErrors', {})
      return dispatch('authModule/register', state.formData, {root: true})
        .then((response) => {
          dispatch('setFormSuccess')
          return response
        })
        .catch((error) => {
          dispatch('setFormFailure', error)
          throw error
        })
    }
  }
})
