import axios from 'axios'
import localStore from 'store'

let root = window.rooturl
export default {
  name: 'authModule',
  namespaced: true,
  state: {
    me: null
  },
  mutations: {
    setMe (state, me) {
      localStore.set('me', me)
      state.me = me
    },
    clearMe (state) {
      localStore.remove('me')
      localStore.remove('jwt')
      delete axios.defaults.headers.common['Authorization']
      state.me = false
    }
  },
  getters: {},
  actions: {
    startupCheck ({dispatch, commit, state}) {
      let me = localStore.get('me')
      let jwt = localStore.get('jwt')

      if (me && jwt) {
        commit('setMe', me)
        axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`
      } else {
      }
    },
    login ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/login`, data)
        .then((response) => {
          let data = response.data
          if (data.access_token) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${data.access_token}`
            localStore.set('jwt', data.access_token)
            return dispatch('me')
          } else {
            delete axios.defaults.headers.common['Authorization']
          }
          return response
        })
        .catch(({response: {data}}) => {
          throw data
        })
    },
    register ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/register`, data)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    },
    logout ({dispatch, commit, state}) {
      return new Promise((resolve, reject) => {
        commit('clearMe')
        resolve(true)
      })
    },
    refresh ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/refresh`, data)
        .then((response) => {
          let data = response.data
          if (data.access_token) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${data.access_token}`
            localStore.set('jwt', data.access_token)
            return dispatch('me')
          } else {
            delete axios.defaults.headers.common['Authorization']
          }
          return response
        })
        .catch(({response: {data}}) => {
          throw data
        })
    },
    update ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/update`, data)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    },
    me ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/me`)
        .then((response) => {
          console.log(response.data)
          commit('setMe', response.data)
          return response
        })
        .catch(({response: {data}}) => {
          throw data
        })
    },
    verifyEmail ({dispatch, commit, state}, {token}) {
      return axios.post(`${root}/api/auth/verifyemail/${token}`)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    },
    resendVerification ({dispatch, commit, state}, {email}) {
      return axios.post(`${root}/api/auth/resendverification/?email=${email}`)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    },
    verifyPasswordResetToken ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/verifyresettoken/`, data)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    },
    requestPasswordReset ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/requestpasswordreset/`, data)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    },
    resetPassword ({dispatch, commit, state}, data) {
      return axios.post(`${root}/api/auth/resetpassword/`, data)
        .then((response) => response)
        .catch(({response: {data}}) => {
          throw data
        })
    }
  }
}