export default {
  data () {
    return {
      formData: {},
      formMessages: {
        errors: {},
        submitMessage: 'Save', //changed internally, use default in components
        submitColor: 'primary', //changed internally, use default in components
        submitMessageSaving: 'Saving...', //component changeable
        submitMessageSuccess: 'Saved!',//component changeable
        submitMessageDefault: null,//component changeable
        submitColorSuccess: 'success',//component changeable
        submitColorSaving: 'disabled',//component changeable
        submitColorDefault: null,//component changeable
        redirect: false,
        submitting: false,
      }
    }
  },
  created () {
    this.formMessages.submitMessageDefault = this.formMessages.submitMessage
    this.formMessages.submitColorDefault = this.formMessages.submitColor
  },
  methods: {
    submitForm (promise) {
      this.formMessages.submitMessage = this.formMessages.submitMessageSaving
      this.formMessages.submitColor = this.formMessages.submitColorSaving
      this.formMessages.submitting = true
      return promise
        .then((data) => {

          this.formMessages.submitMessage = this.formMessages.submitMessageSuccess
          this.formMessages.submitColor = this.formMessages.submitColorSuccess
          this.formMessages.submitting = false
          setTimeout(() => {
            this.formMessages.submitMessage = this.formMessages.submitMessageDefault
            this.formMessages.submitColor = this.formMessages.submitColorDefault
          }, 3000)
          return data
        })
        .catch(errors => {
          this.formMessages.errors = errors
          this.formMessages.submitMessage = this.formMessages.submitMessageDefault
          this.formMessages.submitColor = this.formMessages.submitColorDefault
          this.formMessages.submitting = false
          throw errors
        })
    }
  },
  computed: {
    errorMessage () {
      if (this.formMessages.errors.message) {
        return this.formMessages.errors.message
      }
      return false
    },
    fieldErrors (field) {
      return (field) => {
        if (this.formMessages.errors.errors && this.formMessages.errors.errors && this.formMessages.errors.errors[field]) {
          return this.formMessages.errors.errors[field]
        }
        return []
      }
    },
    submitMessage () {
      return this.formMessages.submitMessage
    },
    submitColor () {
      return this.formMessages.submitColor
    },
    submitting () {
      return this.formMessages.submitting
    },
  }
}