export const laravelErrorsMapper = function (fields) {
  return fields.reduce((accumulator, field) => {
    accumulator[`${field}Errors`] = function () {
      const errors = []
      if (this.errors && this.errors.errors && this.errors.errors[field]) return this.errors.errors[field]
      return errors
    }
    return accumulator
  }, {})
}

export default (input) => {
  let data = {
    ...{
      fields: [],
      formData: {},
      dispatch: '',
      redirect: false,
      submitMessage: null,
      submitMessageDefault: 'Save',
      submitMessageSaving: 'Saving...',
      submitMessageSuccess: 'Saved!',
      submitting: false,
      errors: false,
      submitColor: 'primary',
      submitColorDefault: 'primary',
      submitSuccessColor: 'success'
    },
    ...input
  }

  return {
    data () {
      return data
    },
    created () {
      this.submitMessage = this.submitMessageDefault
    },
    methods: {
      submit () {
        this.SubmitStarted()
        return this.$store.dispatch(this.dispatch, this.formData)
          .then(this.submitSucceeded)
          .catch(this.submitFailed)
      },
      SubmitStarted () {
        this.submitting = true
        this.errors = false
        this.submitMessage = this.submitMessageSaving
      },
      submitSucceeded (response) {
        console.log(response)
        this.submitting = false
        this.submitMessage = this.submitMessageSuccess
        this.submitColor = this.submitSuccessColor
        this.submitRedirect()
      },
      resetForm () {
        this.submitMessage = this.submitMessageDefault
        this.submitColor = this.submitColorDefault
      },
      submitRedirect () {
        if (this.redirect) {
          setTimeout(() => {
            this.$router.push(this.redirect)
          }, 1000)
        } else {
          setTimeout(() => {
            this.resetForm()
          }, 2000)
        }
      },
      submitFailed (err) {
        this.errors = err
        this.submitting = false
        this.submitMessage = this.submitMessageDefault
        // throw err
      }
    },
    computed: {
      ...laravelErrorsMapper(data.fields)
    }
  }
}
