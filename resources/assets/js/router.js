import Vue from 'vue'
import VueRouter from 'vue-router'

const HomePage = () => import(/* webpackChunkName: "home-page" */ './pages/home-page.vue')

Vue.use(VueRouter)

const routes = [
  {path: '/', name: 'home', component: HomePage, meta: {auth: true}},
]

export default function (inputRoutes = [], store) {
  let router = new VueRouter({
    mode: 'history',
    routes: [...inputRoutes, ...routes]
  })

  router.beforeEach((to, from, next) => {
    console.log('beforeeach', to)
    if (!store.state.authModule.me && to.meta.auth) {
      next({name: 'login', params: {redirect: to}})
    } else {
      //Set the sidebar if defined in routes
      if (to.meta.sidebar) {
        store.commit('sidebarModule/setCurrent', to.meta.sidebar)
      } else {
        store.commit('sidebarModule/setCurrent', false)
      }
      next()
    }
  })

  return router
}