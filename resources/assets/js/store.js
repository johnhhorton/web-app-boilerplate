import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

let root = window.rooturl
export default function storeSetup (modules) {
  let appStore = {
    modules: {
      ...modules
    }
  }
  return new Vuex.Store(appStore)
}