@component('mail::message')
# Email Verification
Click the following link to verify your email and sign in.
@component('mail::button', ['url' => route('home',[]) . "#/auth/verify?token={$email_token}"])
Verify Email
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent