@component('mail::message')
# Password Reset
Click the following link to reset your password
@component('mail::button', ['url' => route('home',[]) . "#/auth/password-reset/?token={$password_reset_token}"])
Reset Password
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent