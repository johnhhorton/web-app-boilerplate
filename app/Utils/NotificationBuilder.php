<?php

namespace App\Utils;

use App\Notifications\GeneralNotification;

class NotificationBuilder
{
    public static function build(
        $message,
        $link = null,
        $icon = null,
        $type = "dropdown",
        $contentType = "text")
    {
        return new GeneralNotification([
            'message' => $message,
            'type' => $type,
            'icon' => $icon,
            'link' => $link,
            'contentType' => $contentType
        ]);
    }
}