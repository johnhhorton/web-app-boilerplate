<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $newSite = [
            'id'          => $this->id,
            'name'        => $this->name,
            'description' => $this->description,
            'owner_id'    => $this->owner_id,
            'owner_type'  => $this->owner_type,
            'created_at'  => $this->updated_at,
            'updated_at'  => $this->updated_at,
        ];

        return $newSite;
    }
}
