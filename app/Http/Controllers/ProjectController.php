<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Project;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use \Tymon\JWTAuth\Facades\JWTAuth;
use App\Notifications\GeneralNotification;
use App\Utils\NotificationBuilder;

class ProjectController extends Controller
{
    private $user = null;

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $filter = (string)$request->query('filter');
        $ids = explode(',', $filter);
        $user = auth()->user();
        $projects = $user->projects()
            ->when($filter, function (Builder $query) use ($ids) {
                return $query->whereIn('id', $ids);
            })
            ->get();

//        return response($projects);
        return response(ProjectResource::collection($projects));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $project = new Project($request->all());
        $project->save();
        auth()->user()->notify(NotificationBuilder::build(
            "Project {$project->name} created",
            [
                'name' => 'project-single',
                'params' => ['id' => $project->id]
            ]

        ));

        return response(new ProjectResource($project));
    }

    public function show(Project $project)
    {
        $project->tasks;
        return response($project);
    }

    public function update(Request $request, Project $project)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $project->fill($request->all())->save();
        return response(new ProjectResource($project));
    }

    public function destroy(Project $project)
    {
        $id = $project->id;
        $project->delete();

        return response()->json(['id' => $id]);
    }
}
