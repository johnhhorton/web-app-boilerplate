<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = (string)$request->query('filter');
        $ids    = explode(',', $filter);
        $users  = User::with([])
                      ->when($filter, function (Builder $query) use ($ids) {
                          return $query->whereIn('id', $ids);
                      })
                      ->get();
        return response(UserResource::collection($users));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response('delete this route');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     *
     */
    public function show(User $user)
    {
        return response(new UserResource($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     *
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first'    => 'required',
            'last'     => 'required',
            'email'    => 'required|unique:users,email',
            'projects' => 'sometimes|array|exists:projects,id'
        ]);

        $user->fill($request->all())->save();

        return response(new UserResource($user));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('delete this route');
    }
}
