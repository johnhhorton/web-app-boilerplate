<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        return response(auth()->user()->notifications);
    }

    public function clearNotifications()
    {
        auth()->user()->notifications()->delete();
        return response(auth()->user()->notifications);
    }

    public function destroy($id)
    {
        $notification = auth()->user()->notifications()->where('id', $id)->first();
        if ($notification) {
            $id = $notification->id;
            $notification->delete();
            return response(auth()->user()->notifications);
        }
        return response(['message' => 'Unable to delete notification']);
    }
}
