<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HTTP\Resources\TaskResource;
use App\Task;
use App\User;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = (string)$request->query('filter');
        $ids = explode(',', $filter);
        $user = auth()->user();
        $tasks = $user->tasks()
            ->when($filter, function (Builder $query) use ($ids) {
                return $query->whereIn('id', $ids);
            })
            ->get();

        return response($tasks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'project_id' => 'required|integer|exists:projects,id'
        ]);

        $task = new Task($request->all());
        $task->save();

        return response($task);
    }

    public function show(Task $task)
    {
        return response($task);
    }

    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'name' => 'required',
            'project_id' => 'required|integer|exists:projects'
        ]);

        $task->fill($request->all())->save();
        return response($task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $id = $task->id;
        $task->delete();

        return response()->json(['id' => $id]);
    }
}
