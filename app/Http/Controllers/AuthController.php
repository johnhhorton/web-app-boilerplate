<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\AccountVerification;
use App\Notifications\PaswordReset;
class AuthController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api',
            [
                'except' =>
                    [
                        'login',
                        'register',
                        'verify',
                        'resendVerification',
                        'requestPasswordReset',
                        'validatePasswordResetToken',
                        'passwordReset'
                    ]
            ]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            $user = $this->guard()->user();
            if ($user->verified) {
                return $this->respondWithToken($token);
            } else {
                return response()->json(['message' => 'Your email has not been verified', 'code'=>'NOT_VERIFIED'], 500);
            }
        }

        return response()->json([
            'message' => 'Username and Password do not match'
        ], 401);
    }

    /**
     * Register new user
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'first' => 'required|string|max:255',
            'last' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $userData = $request->all();
        $user = new User($userData);
        $email_token = base64_encode($user->email);
        $user->email_token = $email_token;
        $user->save();
        $user->notify(new AccountVerification());
        return response($user);
    }

    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        if (!$user) {
            return response()->json(['message' => 'invalid token'], 500);
        }
        $user->verified = 1;
        $user->email_token = null;
        if ($user->save()) {
            return response()->json(['message' => 'verified']);
        } else {
            return response()->json(['message' => 'failed'], 500);
        }
    }

    public function resendVerification(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255',
        ]);
        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        if (!$user) {
            return response()->json(['message' => 'Account not found'], 500);
        }
        if ($user->verified === 1) {
            return response()->json(['message' => "{$email} has already been verified"], 500);
        }
        $user->notify(new AccountVerification());
        return response()->json(['message' => "Verification email has been sent to {$email}"]);
    }


    /**
     * Get the authenticated User
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Request password reset email
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function requestPasswordReset(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255|exists:users',
        ]);

        $user = User::where('email', $request->input('email'))->first();
        //TODO: this token gets bcrypted. Find out if there's a better method.
        $user->password_reset_token = bcrypt($user->email . date("Y-m-d H:i:s"));
        $user->save();
        $user->notify(new PasswordReset());

        return response()->json([
            'message' => 'Password Reset sent'
        ]);
    }

    /**
     * Request password reset email
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function validatePasswordResetToken(Request $request)
    {
        $this->validate($request,[
            'token'=>'required|string'
        ]);
        $token = $request->input('token');
        $user = User::where('password_reset_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'Token is invalid'
            ], 500);
        }

        return response()->json([
            'message' => 'Token is valid'
        ]);
    }

    public function passwordReset(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255|exists:users',
            'token' => 'required|string',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $user = User::where('password_reset_token', $request->input('token'))->first();
        if (!$user) {
            return response()->json([
                'message' => 'Token is invalid'
            ], 500);
        }

        $user->password = $request->input('password');
        $user->password_reset_token = null;
        $user->save();

        return response()->json([
            'message' => 'Password changed'
        ]);
    }

    /**
     * Update authenticated User
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = $this->guard()->user();
        $this->validate($request, [
            'first' => 'required|string|max:255',
            'last' => 'required|string|max:255',
        ]);
        if ($request->input('password')) {
            $this->validate($request, [
                'password' => 'string|min:6|confirmed',
            ]);
        }
        if ($request->input('email') !== $user->email) {
            $this->validate($request, [
                'email' => 'required|string|email|max:255|unique:users',
            ]);
        }

        $user->fill($request->all())->save();


        return response()->json($user);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}