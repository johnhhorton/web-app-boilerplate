<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable =['name', 'description', 'project_id', 'done'];

    public function project() {
        return $this->belongsTo( 'App\Project' );
    }
}
