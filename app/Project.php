<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['name', 'description', 'url', 'owner_id', 'owner_type', 'owner'];

//	protected $hidden = [];
    public function owner()
    {
        return $this->morphTo('owner');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

//    public function views()
//    {
//        return $this->hasMany('App\View');
//    }
//
//    public function sharedOrganizations()
//    {
//        return $this->belongsToMany('App\Organization', 'project_organization_share', 'project_id', 'organization_id');
//    }
//
//    public function sharedUsers()
//    {
//        return $this->belongsToMany('App\User', 'project_organization_share', 'project_id', 'user_id');
//    }
    //one to many shared with users

    //one to many shared with organizations
}
