
#Laravel SPA Boilerplate
This project aims to provide a complete boilerplate for a Laravel 
single page application(spa).

##Architecture
Backend:
* [Laravel](https://laravel.com/)
* API only access to authentication and data
* [JWT Authentication](https://github.com/tymondesigns/jwt-auth)

Front-end:
* [VueJs](https://vuejs.org/): Framework
* [Vuex](https://vuex.vuejs.org/en/): State management
* [Vue-router](https://router.vuejs.org/en/): routing
* [Vuetify](https://vuetifyjs.com/): UI Library

###Back-end
This project uses the standard Laravel project architecture. Rather than making 
heavy use of view templates, this site exposes all of it's data through the API. 
The API endpoints and controllers should always be [Resource based](https://laravel.com/docs/5.5/controllers#resource-controllers), 
except in unique cases. The front-end expects the resource route pattern for accessing
 models.
 
 ###Front-end
 The front-end is a VueJs driven SPA (single page application). Changing pages is controlled by 
 vue-router, so all user facing routes in the application will need to be configured in the JS.
  Vuex controls as much of the state management as possible. All Vuex operations are split into their respective 
  modules. There are modules for models, pages, and unique components (like forms). Non-model 
  modules do not own model related objects. Instead, all pages and unique component modules should 
  use the specific model's getters to acquire the needed items by id.
  
  Rather than reimplenting styling for each project. This project makes use of the Vuetify UI 
  Library.
##Installation instructions
Make sure that you have PHP 7.1+ and composer installed.
Make sure that you have mysql setup and running locally with a database for this project.

`composer install`

`npm install`

Copy `.env.example` to `.env` and set up the database credentials.

`php artisan migrate` - sets up the database

`php artisan key:generate` - generates app secret key

`php artisan jwt:secret` - generates token signing key

##Development startup instructions
Term1: `php artisan serve` runs on localhost:8000

Term2: `npm run watch` watches and rebuilds js bundle.

##Troubleshooting

###Windows: on register: cURL error 60.
Follow instructions [here](https://laracasts.com/discuss/channels/general-discussion/curl-error-60-ssl-certificate-problem-unable-to-get-local-issuer-certificate/replies/37017). 
Then restart your php server `ctrl+c, php artisan serve`
