const ExtractTextPlugin = require('extract-text-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const webpack = require('webpack')
const path = require('path')
const production = (process.env.NODE_ENV === 'production')
const extractStyleCSS = new ExtractTextPlugin('./public/style.css')

module.exports = {
  entry: {
    index: './resources/assets/js/main.js',
    // polyfill: './js/src/polyfill.js'
  },
  output: {
    path: path.resolve(__dirname, 'public/js/'),
    filename: '[name].js',
    chunkFilename: '[name]-chunk.js',
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {}
          // other vue-loader options go here
        }
      },
      {
        test: /\.css$/,
        use: [{
          loader: 'style-loader', options: {
            sourceMap: true
          }
        }, {
          loader: 'css-loader', options: {
            sourceMap: true
          }
        }]
      },
      {
        test: /\.scss$/,
        use: extractStyleCSS.extract({
          use: [{
            loader: 'css-loader', options: {
              sourceMap: true
            }
          }, {
            loader: 'postcss-loader', options: {
              sourceMap: true
            }
          }, {
            loader: 'sass-loader', options: {
              sourceMap: true
            }
          }]
        })
      },
      {

        test: /\.js$/,
        include: [
          path.resolve(__dirname, 'resources/assets/js'),
        ],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [['es2015', {modules: false}]],
            plugins: [
              'syntax-async-functions',
              'syntax-dynamic-import',
              'transform-object-rest-spread',
              'transform-async-to-generator',
              'transform-regenerator',
              'transform-runtime'
            ]
          }
        }]
      },
      {
        test: /\.(jpe?g|gif|png|svg|eot|woff|woff2|ttf)$/,
        loader: 'file-loader?emitFile=false&name=[path][name].[ext]'
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.vue', '.json'],
    alias: {
      vue: 'vue/dist/vue.common.js',
      utils: 'resources/assets/js/utils'
    },
    modules: [
      path.resolve('./'),
      path.resolve('./node_modules'),
    ]
  },
  plugins: [
    extractStyleCSS,
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: production,
    }),
  ],
  externals: {},
  devtool: 'source-map'
}