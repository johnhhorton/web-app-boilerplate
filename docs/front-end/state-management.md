#State Management in Vue and Vuex
This document covers the reasons and methodology for managing applciation state with 
vuex.

##Why not just use the component state?
In VueJS, components have their own internal state that can manage the data that 
they're working with. This is an acceptable approach for very simple isolated Vue 
components, but it can quickly become unmanageable in a medium to large sized SPA. 
Most of the components, logic, and states are shared to some degree throughout an SPA 
in order to increase manageability, increase code reuse, and keep developers sane. 
When adding all of data to individual components throughout the application through 
api responses, embedded data, or user input, the mess that is can create quickly 
becomes apparent.

## Why Vuex?
Vuex allows a developer to store application state, changes, and login in a central 
location disconnected from the UI. Any persisting change to data, or requests to do 
so, must be handled by Vuex for this approach to be effective. When Vue components 
can reference state and request changes from that central repository, a developer is
 no longer cluttering the UI logic with application logic. It also allows the 
 modules in the state to be reusable throughout the application.
 
 Keep your UI as lean as possible.
 
 ## How to manage state with Vuex.
 At this point, I'm assuming that you have read through the Vuex docs and understand 
 the principals.
 
 Vuex state can be managed using to general areas of approach. single view apps, and 
 multi-view apps.
 
 ### Single view applications
 A single view application is a Vue app where the core application does not span 
 multiple routes/pages. In this situation, it's common, and permissable, to store all 
 of the application state at the root level of the Vuex store.
 
 ### Multi-view Apps
 In multi-view applications, attempting to store all of the application's state in the 
 root store quickly becomes unmaintainable. Instead, make use of Vuex modules.
 
 I recommend creating a new Vuex module for each:
 * Page
 * Route model binding - projects, auth, comments, etc
 * Form - Forms that submit their own data through route/model modules to the api.
 * Independent component - A component that does not rely on others for state.
 
 #### Managing Page State
 Along with the Route and Vue component, create a separate module for each page.
 The page Vue component will then use an action on the module to start the api 
 query (pages should use a request-in-progress flag for user feedback). When the 
 action's promise returns, the Page's template can then make use of the prepared 
 module state to display contents.
 
 Most of the time, a page will contain other reusable components. If these components 
 rely on the page state for data (example: CommentsList relying on the Post ID), do 
 not create a separate module for the child components. If the child components do not 
 need their own state management, treat them as dumb/visual components and pass in the 
 data via props. This will cascade down until a child component that does need its' own 
 state receives relevant props and makes use of its' own module.
 
 #### Managing Route Model Binding Modules.
 These modules are used in REST APIs to gain access to crud based operations on the route. 
 Rather than making queries directly from a page module, or component, API connecting 
 modules should stand separately. Most of the time, a component or other module will need 
 to access data from one of those routes. In order to do so, simply call the module's 
 querying actions to get the data. A new module should be created for every module exposed 
 by the API.
 
 #### Managing Form State
 Forms can be intensely complicated and make use of a lot of reusable logic. 
 Forms that reach out to the API should always maintain their own state, as there may 
 be several round trips to the API for things like validation, authorization, additional 
 data, and success feedback. This project contains some logic that helps make managing 
 asynchronous forms a bit more bearable.
 
 See forms.md for more documentation on how to use these utilities.
 
 #### Managing independent component state
 Just like forms, independent components that may not explicitly rely on their parent for 
 data still need some sort of state management. Sometimes this state management can be 
 handled effectively internally (useful if there are many of them running concurrently), 
 but often you will want to consider creating a separate module speciically for that component.
 