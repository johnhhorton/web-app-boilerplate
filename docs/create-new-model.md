#Creating a new model
This boilerplate's tooling is centered around models. 
Below is an example of how to get going fast with new 
model generation.

##Laravel
###Create the model and migration
```
php artisan make:model Task --migration
```

###Create the controller. 

*Note: resource created the resource 
methods required for easy front-end integration*
```
php artisan make:controller TaskController --resource
```

###Create the Resource Route
Add your resource route to `/routes/api.php`. Use the only condition to only include the api 
based routes.  Make sure to delete the extra unused methods from the controller.
```PHP
Route::resource('task', 'TaskController', [
        'only' => [
            'index',
            'show',
            'store',
            'update',
            'destroy'
        ]
    ]);
```

###Create the API Resource (optional, but recommended)

Resources are used to manipulate/filter a model's data
before sending via JSON response. This is particularly 
important if you want to return a list of IDs for a 
relationship rather than the objects.
```
php artisan make:resource TaskResource
```
###Create a Factory for the model
Factories are used in testing and seeding to generate 
a valid model.
```
php artisan make:factory TaskFactory --model=Task
```

###Create a Seeder for the model
Seeders are used to fill the database with development 
example data.
```
php artisan make:seeder TasksTableSeeder
```
Or if your seeder is relational, make use of the factory 
in a seeder that should be connected to this model.

###Create a Test class for the API
```
php artisan make:test TaskTest
```
##Vue
With the back end model functionality stubbed out, a new 
feature can be added to handle this model.

###Add feature directories
Under `/resources/assets/js/src/features`:

Create a new feature folder `/tasks`

Under tasks, add folders: `/components` `pages` `plugins` `store(optional)`

###Add feature standard files
`index.js` exports everything needed by the feature to the 
main application.
```javascript
// tasks/index.js
import routes from './routes'
import tasksService from './plugins/tasks-service'

export default {
  routes,
  plugins: [
    tasksService
  ]
}
```

`routes.js` links all of your features routes to pages.
Included are some example routes. These are all optional.

Pages are imported using this syntax to load page components 
asynchronously via webpack chunks. Make sure to name your chunks 
uniquely. Routes are resolved by the first match, so make sure 
any routes that may conflict (like `/create` and `:id`) are ordered 
appropriately.
```javascript
// tasks/routes.js
const TaskSinglePage = () => import(/* webpackChunkName: "task-single-page" */ './pages/task-single-page')
const TaskCreatePage = () => import(/* webpackChunkName: "task-create-page" */ './pages/task-create-page')

export default [
  {path: '/task/create', name: 'task-create', component: TaskCreatePage, meta: {auth: true}},
  {path: '/task/:id', name: 'task-single', component: TaskSinglePage, meta: {auth: true}}
]
```

`plugins/tasks-service.js` Creates a service plugin through which the 
Vue application can interact with the API for this model.
```javascript
// plugins/tasks-service.js
import { createModelServicePlugin } from 'utils/modelService'

export default createModelServicePlugin({
  name: 'tasks',
  url: `${window.rooturl}/api/task`
})
```
###Pages and Components
Pages and components are optional, as they may change based on model.
Pages are used by `routes.js` for this specific feature.
Components can be used by this feature other others.

###Register feature
TODO: write registration handler utility and show example.