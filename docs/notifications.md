#Notifications
Laravel has a built in notification system that can handle 
a range of notification types (email, database, sms, etc). 
When creating a new notification, make use of the built in 
notification system.

##Create a new email notification
Creating an email notification should always make use of 
markdown. This is easy to set up with the make:notification 
command.
```
php artisan make:notification InvoicePaid --markdown=mail.invoice.paid
```
This command will automatically create the new notification 
in `app/Notifications/` as well as the related markdown file 
at `resources/views/mail/` based on the view location field 
used in the command.

##Customizing a notification
See [the Laravel docs](https://laravel.com/docs/5.6/notifications#creating-notifications) 
for usage and customization.
