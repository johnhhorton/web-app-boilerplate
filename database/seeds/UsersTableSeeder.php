<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Create default user
        $user = new \App\User([
            'first' => env("TEST_USER_FIRST", "first"),
            'last' => env("TEST_USER_LAST", "last"),
            'email' => env("TEST_USER_EMAIL", "developer@symphonyagency.com"),
            'password' => env("TEST_USER_PASS", "password"),
            'verified' => true
        ]);
        $user->save();

        $user->projects()->saveMany(
            factory(App\Project::class, 4)->create()->each(function (App\Project $project) use ($user) {
                $project->tasks()->saveMany(
                    factory(App\Task::class, 10)->create());
            })
        );

        //Create example users
        factory(App\User::class, 10)->create()->each(function (App\User $user) {
            $user->projects()->saveMany(

                factory(App\Project::class, 4)->create()->each(function (App\Project $project) use ($user) {
                    $project->tasks()->saveMany(
                        factory(App\Task::class, 10)->create());
                }));
        });
    }
}
