<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'name' => $faker->text(25),
        'description' => $faker->text(),
        'done' => $faker->boolean()
    ];
});
